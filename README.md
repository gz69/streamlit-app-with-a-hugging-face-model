
# Streamlit Text Generation App using Hugging Face Model

This project develops a web application using Streamlit, integrating it with an open-source Large Language Model (LLM) from Hugging Face. The application facilitates generating text based on user input, utilizing OpenAI's pre-trained GPT-2 model.

## Live Project

Access the live application here: [Streamlit Text Generation App](https://ds655-gpt2.streamlit.app/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Python 3.6 or later. Download Python [here](https://www.python.org/downloads/).
- A Windows/Linux/Mac machine with a stable internet connection.
- Familiarity with Hugging Face model documentation.

### Installation

1. **Clone the Repository**

   Open a terminal and run the following commands to clone the repository and navigate to the project directory:

   ```bash
   git clone https://gitlab.com/gz69/streamlit-app-with-a-hugging-face-model.git
   cd streamlit-app-with-a-hugging-face-model
   ```

2. **Install Dependencies**

   Install the necessary Python libraries using pip:

   ```bash
   pip install -r requirements.txt
   ```

   This command installs all required dependencies, including Streamlit and Transformers.

### Creating the Streamlit App

1. **Set Up Your App File**

   Create a new Python file named `app.py`. This file will contain your Streamlit application code.

2. **Import Libraries**

   At the beginning of `app.py`, import the necessary libraries:

   ```python
   import streamlit as st
   from transformers import pipeline
   ```

3. **Initialize the Hugging Face Model**

   Use the `pipeline` function to load the GPT-2 model:

   ```python
   generator = pipeline('text-generation', model='gpt2')
   ```

4. **Design the Streamlit Interface**

   Create the user interface for your application. Include an input area and a button to trigger text generation:

   ```python
   st.sidebar.title("Input Options")
   input_text = st.sidebar.text_input("Enter some text")
   generate_button = st.sidebar.button("Generate")
   ```

5. **Implement Text Generation**

   Generate text when the user clicks the "Generate" button:

   ```python
   if generate_button:
       with st.spinner("Generating text..."):
           output_text = generator(input_text)[0]["generated_text"]
       st.subheader("Generated Text:")
       st.write(output_text)
   ```

### Running the App

Execute the following command in your terminal to run the Streamlit app:

```bash
streamlit run app.py
```

### Deployment

Deploy your app using Streamlit Sharing, Heroku, or AWS. Ensure your application is accessible via a browser for users to interact with.